"""Testing script for principal-fft-torch"""
#! /usr/bin/env python3

# Python imports
import os
import sys
import logging

# Module imports
import torch
import numpy as np

# Local imports
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
import pft

def test_pft_torch():

    # Sets up random number generator
    rng = torch.Generator(device="cpu")
    rng.manual_seed(42)
    
    ###### Tests simple sine signal ######
    # Assigns variables 
    num_steps = 100
    num_samples = 1000
    num_coefs = 10
    amp = torch.rand((num_samples, 1), generator=rng) * 100
    freq = torch.rand((num_samples, 1), generator=rng) * 80
    t = torch.linspace(0, 2 * torch.pi, num_steps)

    signal = amp * torch.sin(freq * t)

    pfa = pft.PFT(num_coefs)
    pfa.fit(signal)

    # Checks if indexes of maximum frequency match
    max_idx = torch.tensor([43, 35, 45, 36, 46, 28, 41, 47, 44, 20])
    assert torch.all(max_idx == pfa.idx)


    ###### Tests inverse of a signal ######
    tol = 1e-2
    num_steps = 1000
    num_coefs = 500
    num_princ_coefs = 450

    coefs = torch.rand(num_coefs, generator=rng) * torch.linspace(0, 1, num_coefs)
    freqs = torch.rand(num_coefs, generator=rng) * 2 * torch.pi
    t = torch.linspace(0, 2 * torch.pi, num_steps)

    signal = torch.zeros((1, num_steps))
    for i, coef in enumerate(coefs):
        signal += coef * torch.sin(freqs[i] * t)

    pfa = pft.PFT(num_princ_coefs)
    pfa.fit(signal)

    # Reconstructs the signal from principal Fourier components
    p_coefs = pfa.transform(signal)
    r_signal = pfa.ifit(p_coefs, t)

    diff = torch.abs(signal - r_signal)
    mean_diff = torch.mean(diff[signal != 0] / torch.abs(signal[signal != 0]))
    assert mean_diff < tol

    logger.debug("\ndiff:\t%f\ntol:\t%f" % (mean_diff, tol))

def test_pft_numpy():

    # Sets up random number generator
    rng = np.random.default_rng(42)

    ###### Tests simple sine signal ######
    # Assigns variables 
    num_samples = 1000
    num_steps = 100
    num_coefs = 1

    amp = rng.random((num_samples, 1))
    freq = rng.random((num_samples, 1))
    t = np.linspace(0, 2 * torch.pi, num_steps).reshape(1, -1)

    signal = amp * np.sin(freq * t)

    pfa = pft.PFT(num_coefs, use_torch=False)
    pfa.fit(signal)

    # Checks if indexes of maximum frequency match
    max_idx = np.argsort(np.abs(np.fft.rfft(signal[0, :])))[-1]
    assert np.all(max_idx == pfa.idx)

    ###### Tests inverse of a signal ######
    tol = 5e-2
    num_steps = 1000
    num_coefs = 100
    num_princ_coefs = 90

    coefs = rng.standard_normal(num_coefs) * np.linspace(0, 1, num_coefs)
    freqs = rng.standard_normal(num_coefs) * 2 * np.pi
    t = np.linspace(0, 2 * np.pi, num_steps)

    signal = np.zeros((1, num_steps))
    for i, coef in enumerate(coefs):
        signal += coef * np.sin(freqs[i] * t)

    pfa = pft.PFT(num_princ_coefs, use_torch=False)
    pfa.fit(signal[[0], :])

    # Reconstructs the signal from principal Fourier components
    p_coefs = pfa.transform(signal)
    r_signal = pfa.ifit(p_coefs, t)

    diff = np.abs(signal - r_signal)
    mean_diff = np.mean(diff[signal != 0] / np.abs(signal[signal != 0]))
    assert mean_diff < tol

    logger.debug("\ndiff:\t%f\ntol:\t%f" % (mean_diff, tol))

if __name__ == "__main__":

    logger = logging.getLogger(__name__)
    logger.setLevel("DEBUG")
    logger.addHandler(logging.StreamHandler(sys.stdout))
    test_pft_torch()
    test_pft_numpy()
    logger.info(f"{__file__} passed!")
